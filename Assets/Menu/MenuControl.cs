﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour {

	public void nuevaPartida(){
		SceneManager.LoadScene ("Scene1");
	}

	public void cargarJuego(){

	}

	public void creditos(){
		SceneManager.LoadScene ("ScrollingCredits");
	}

	public void opciones(){
		SceneManager.LoadScene ("OpcionesFran");
	}

	public void salir(){
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}
		
	public void volver(){
		SceneManager.LoadScene ("MenuFran");
	}
}
