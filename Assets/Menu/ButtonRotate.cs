﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonRotate : MonoBehaviour {

	// Transition state, t in [0,1]
	public bool flipped = false;
	private bool transitioning = false;
	private float t = 1.0f;

	void SetRotation()
	{
		float time = (flipped) ? (t) : (1.0f - t);
		float angle = time * 360.0f;
		transform.eulerAngles = new Vector3 (angle, 0, 0);
	}

	public void OnMouseOver()
	{
		Debug.Log ("Mouse over");
		if (transitioning == true)
			return;

		Debug.Log ("Mouse over trns. starting");
		flipped = true;
		t = 0.0f;
		transitioning = true;
	}
		
	void Update ()
	{
		if (transitioning)
		{
			float dt = Time.unscaledDeltaTime;
			t += 2.0f * dt; // => duration 0.5s
			if (t > 1.0f) {
				t = 1.0f;
				transitioning = false;
				Debug.Log ("Transition finished");
			}
			SetRotation ();
		}
	}
}
