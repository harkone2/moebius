﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Script that makes sure the GameManager is available 
 * Should be attached to e.g. the MainCamera of each scene
*/

public class Loader : MonoBehaviour {

	public GameObject gameManager;

	void Awake () {
		// Check if singleton (static instance) exists
		if (GameManager.instance == null)
			Instantiate (gameManager);
	}

}
