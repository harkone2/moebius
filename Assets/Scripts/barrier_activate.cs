﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barrier_activate : MonoBehaviour {

	private Animator gateAnim;
	private AudioSource gateSound;

	// Use this for initialization
	void Start () {
		gateAnim = GetComponent<Animator> ();
		gateSound = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void DoorOpen() {
		gateAnim.SetBool ("Open", true);
		gateSound.Stop ();
		gateSound.PlayDelayed (0.4f);
	}

	public void DoorClose() {
		gateAnim.SetBool ("Open", false);
		gateSound.Stop ();
		gateSound.PlayDelayed (0.4f);
	}
}
