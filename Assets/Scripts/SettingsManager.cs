﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {

	public static string SOUND_VOLUME = "SOUND_VOLUME";
	public Slider soundSlider;

	void Start() {
		soundSlider.value = PlayerPrefs.GetFloat (SOUND_VOLUME, 0.8f);
	}

	public void SoundsSliderChanged(float value)
	{
		Debug.Log ("Volume: " + value);
		PlayerPrefs.SetFloat (SOUND_VOLUME, value);
		AudioListener.volume = value;
	}
}
