﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomGravity : MonoBehaviour {

	// True for upper world, false for lower
	public bool isUpper;
	private FlipWorld worldState;

	// Turn off standard gravity
	void Start () {
		foreach(Rigidbody2D rb in gameObject.GetComponentsInChildren<Rigidbody2D>()) {
			rb.gravityScale = 0.0f;
		}
		worldState = this.GetComponentInParent<FlipWorld>();
	}
	
	// Update is called once per frame
	void Update () {
		bool flipped = worldState.flipped;
		float defaultGravity = isUpper ? -9.81f : 9.81f;
		float gravity = flipped ? -defaultGravity : defaultGravity;

		foreach(Rigidbody2D rb in gameObject.GetComponentsInChildren<Rigidbody2D>()) {
			rb.AddForce (Vector3.up * gravity);
		}
	}
}
