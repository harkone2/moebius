﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elevator : MonoBehaviour {

	Rigidbody2D platform;

	// Use this for initialization
	void Start () {
		platform = GetComponent<Rigidbody2D> ();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		Debug.Log ("OnCollisionEnter!");
		GameObject obj = other.gameObject;
		if (obj.CompareTag("Player"))
		{
			// Disable movement controls
			ToggleMovement m = obj.GetComponent<ToggleMovement> ();
			m.SetActive (false);
			GameManager.instance.ChangeLevel ();
		}
	}
}
