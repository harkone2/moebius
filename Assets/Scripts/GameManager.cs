﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;   // Static instance of GameManager which allows it to be accessed by any other script.
	private ScreenFade fadeScript;
	private int level = -1;                      // Current level number, based on build settings
	private IEnumerator coroutine;
	private bool transitioning = false; 		 // prevent simultaneous level transitions

	void Awake()
	{
		// enforce singleton pattern
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);

		// do not destroy when reloading scene
		DontDestroyOnLoad(gameObject);

		fadeScript = GetComponent<ScreenFade> ();

		// call the InitGame function to initialize the first level 
		InitGame();
	}

	// Initializes the game for each level.
	void InitGame()
	{
		level = SceneManager.GetActiveScene ().buildIndex;
		Debug.Log ("InitGame called on level " + level);

		// Set global audio volume
		AudioListener.volume = PlayerPrefs.GetFloat(SettingsManager.SOUND_VOLUME, 0.8f);
		Debug.Log("Global audio volume set to " + AudioListener.volume);
	}
		
	void OnEnable()
	{
		SceneManager.sceneLoaded += OnLevelTransition;
	}

	void OnDisable()
	{
		SceneManager.sceneLoaded -= OnLevelTransition;
	}

	// Catch level transitions, update index
	void OnLevelTransition(Scene scene, LoadSceneMode mode)
	{
		level = scene.buildIndex;
		Debug.Log("Loaded level " + level);
		transitioning = false;
	}
		
	void Update()
	{
		// Restart if not in menu
		if (Input.GetKeyDown (KeyCode.R) && level >= 4)
		{
			StartCoroutine (RestartLevelRoutine ());
		}
	}

	public void ChangeLevel() {
		if (transitioning)
			return;
		
		transitioning = true;
		coroutine = ChangeLevelRoutine ();
		StartCoroutine (coroutine);
	}

	IEnumerator RestartLevelRoutine() {
		//yield return new WaitForSeconds (0.1f);
		float fadeTime = fadeScript.BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene (level);
		fadeTime = fadeScript.BeginFade(-1);
		yield return new WaitForSeconds (fadeTime);
	}

	IEnumerator ChangeLevelRoutine() {
		yield return new WaitForSeconds (0.6f);
		float fadeTime = fadeScript.BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		int levels = SceneManager.sceneCountInBuildSettings;
		level = (level + 1) % levels;
		Debug.Log ("Transitioning to level " + level);
		SceneManager.LoadScene (level);
		fadeTime = fadeScript.BeginFade(-1);
		yield return new WaitForSeconds (fadeTime);
	}
}
