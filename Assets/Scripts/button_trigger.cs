﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class button_trigger : MonoBehaviour {

	public GameObject buttonBase;
	public Sprite upperSprite, lowerSprite;

	// Functions that are triggered by the button
	public UnityEvent buttonOnEvent;
	public UnityEvent buttonOffEvent;

	private SpriteRenderer rend;
	private AudioSource audioSrc;

	// Id-tag mapping of colliding objects
	private Dictionary<int, string> collidingObjects = new Dictionary<int, string> ();

	void Start () {
		rend = buttonBase.GetComponent<SpriteRenderer> ();
		audioSrc = this.GetComponent<AudioSource> ();
	}

	void OnTriggerEnter2D(Collider2D other) {
		int id = other.gameObject.GetInstanceID ();
		collidingObjects [id] = other.gameObject.tag;

		// State transition?
		bool alreadyPressed = (rend.sprite == lowerSprite);
		if (!alreadyPressed && (collidingObjects.ContainsValue ("Player") || collidingObjects.ContainsValue ("Item"))) {
			rend.sprite = lowerSprite;
			EmitSound ();
			buttonOnEvent.Invoke ();
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		int id = other.gameObject.GetInstanceID ();
		collidingObjects.Remove (id);

		// State transition?
		bool alreadyReleased = (rend.sprite == upperSprite);
		if (!alreadyReleased && (!collidingObjects.ContainsValue ("Player") && !collidingObjects.ContainsValue ("Item"))) {
			rend.sprite = upperSprite;
			EmitSound ();
			buttonOffEvent.Invoke ();
		}
	}

	void EmitSound() {
		audioSrc.Play ();
	}
}
