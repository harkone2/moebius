﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class springManager : MonoBehaviour {
	public Animator anim;
	private HashSet<GameObject> collidingObjects = new HashSet<GameObject> ();
	public float shootingSpeed;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D other){
		collidingObjects.Add (other.gameObject);
	}

	void OnCollisionExit2D(Collision2D other){
		collidingObjects.Remove (other.gameObject);
	}

	public void shootMe (){
		foreach(GameObject obj in collidingObjects){
			Rigidbody2D rb = obj.GetComponent<Rigidbody2D> ();
			rb.AddForce (this.transform.up*shootingSpeed);
		}

	}
}
