﻿using UnityEngine;
using System.Collections;

public class dontDestroyOnload : MonoBehaviour {
	static bool AudioBegin = false; 
	void Awake()
	{
		AudioSource audio = GetComponent<AudioSource>();
		if (!AudioBegin) {
			audio.Play ();
			DontDestroyOnLoad (gameObject);
			AudioBegin = true;
		} 
	}
	void Update () {
		AudioSource audio = GetComponent<AudioSource>();
		if(Application.loadedLevelName == "Scene1")
		{
			audio.Stop();
			AudioBegin = false;
		}
	}
}