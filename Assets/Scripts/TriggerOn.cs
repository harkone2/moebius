﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOn : MonoBehaviour {
	
	public Animator anim;

	// Id-tag mapping of colliding objects
	private Dictionary<int, string> collidingObjects = new Dictionary<int, string> ();

	void OnCollisionEnter2D(Collision2D coll) {
		int id = coll.gameObject.GetInstanceID ();
		collidingObjects [id] = coll.gameObject.tag;

		if (collidingObjects.ContainsValue ("Player") || collidingObjects.ContainsValue ("Item")) {
			anim.SetBool ("Open", true);
		}
	}

	void OnCollisionExit2D(Collision2D coll) {
		int id = coll.gameObject.GetInstanceID ();
		collidingObjects.Remove (id);

		if (!collidingObjects.ContainsValue ("Player") && !collidingObjects.ContainsValue ("Item")) {
			anim.SetBool ("Open", false);
		}
	}

}
