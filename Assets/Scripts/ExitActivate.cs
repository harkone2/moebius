﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitActivate : MonoBehaviour {

	// Always a pair of exits
	public GameObject otherExit;
	private ExitActivate otherExitScript;

	public Animator lightAnimator;
	//public Animator plateAnimator;

	void Start () {
		ExitActivate[] scripts = otherExit.GetComponentsInChildren<ExitActivate> ();
		if (scripts.Length != 1)
			throw new Exception ("Exit hierarchy incorrectly set up!");
		otherExitScript = scripts [0];
	}

	public bool IsActive() {
		return lightAnimator.GetBool ("LightIsOn");
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			lightAnimator.SetBool ("LightIsOn", true);

			// TODO: rigorously prevent concurrent level changes
			if (otherExitScript.IsActive ())
				GameManager.instance.ChangeLevel ();
		}
	}

	void OnCollisionExit2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player")
			lightAnimator.SetBool ("LightIsOn", false);
	}
}
