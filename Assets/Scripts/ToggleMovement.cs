﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets._2D;

[RequireComponent(typeof(Platformer2DUserControl))]
public class ToggleMovement : MonoBehaviour {

	Platformer2DUserControl movementScript;
	Animator animator;

	void Start() {
		movementScript = gameObject.GetComponent<Platformer2DUserControl> ();
		animator = gameObject.GetComponent<Animator> ();
	}

	// Called from FlipWorld.cs
	public void SetActive(bool value) {
		movementScript.enabled = value;
		if (!value)
			animator.SetFloat ("Speed", 0.0f);
	}

}
