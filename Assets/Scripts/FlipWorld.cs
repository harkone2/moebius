﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipWorld : MonoBehaviour {

	// Access to children (upper/lower half of world)
	// Used to toggle physics simulation on/off
	GameObject world;

	// For toggling controlled player
	public GameObject playerUp;
	public GameObject playerDown;

	// Transition state, t in [0,1]
	public bool flipped = false;
	private bool transitioning = false;
	private float t = 1.0f;

	// Use this for initialization
	void Start () {
		world = gameObject;
	}

	void SetWorldRotation() {
		float time = (flipped) ? (t) : (1.0f - t);
		float angle = time * 180.0f;
		transform.eulerAngles = new Vector3 (angle, 0, 0);
	}

	void DisablePhysics() {
		Time.timeScale = 0.0f;
	}

	void EnablePhysics() {
		Time.timeScale = 1.0f;
	}

	void ToggleActivePlayer() {
		playerUp.GetComponent<ToggleMovement> ().SetActive ((flipped) ? false : true);
		playerDown.GetComponent<ToggleMovement> ().SetActive ((flipped) ? true : false);
	}
	
	// Update is called once per frame
	void Update () {
		// Abort previous transition, start new one
		if (Input.GetKeyDown ("space")) {
			flipped = !flipped;
			t = 0.0f;
			transitioning = true;
			DisablePhysics ();
		}

		if (transitioning) {
			float dt = Time.unscaledDeltaTime;
			t += 2.0f * dt; // => duration 0.5s
			if (t > 1.0f) {
				t = 1.0f;
				transitioning = false;
				EnablePhysics ();
				ToggleActivePlayer ();
			}
			SetWorldRotation ();
		}
	}
}
