﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attraction : MonoBehaviour {
	public float attract;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerStay2D (Collider2D other){
		Rigidbody2D rb = other.gameObject.GetComponent<Rigidbody2D> ();
		rb.AddForce (-this.transform.up * attract); // down in local coordinate frame of force field
	}

}
